#include "doctest.h"

#include <string>

#include "sources/Point.hpp"
#include "sources/Character.hpp"
#include "sources/Team.hpp"

using namespace doctest;
using namespace ariel;

static bool s_randInitialized = false;

int randInteger(int min, int max)
{
    if (!s_randInitialized) {
        std::srand((unsigned)time(nullptr));
        s_randInitialized = true;
    }

    return (int)(std::rand() % (max - min + 1)) + min;
}

TEST_SUITE("Point unit test") {
    TEST_CASE("build")
    {
        Point p1;
        Point p2(3, 4);
        Point p3(-3, 4);
        Point p4(3, -4);
        Point p5(-3, -4);

        std::cout << "Point 1: " << p1.print() << std::endl;
        std::cout << "Point 2: " << p2.print() << std::endl;
        std::cout << "Point 3: " << p3.print() << std::endl;
        std::cout << "Point 4: " << p4.print() << std::endl;
        std::cout << "Point 5: " << p5.print() << std::endl;

        CHECK_EQ(p1.getX(), 0);
        CHECK_EQ(p1.getY(), 0);
        CHECK_EQ(p1, Point());
        CHECK_EQ(p2.getX(), 3);
        CHECK_EQ(p2.getY(), 4);
        CHECK_EQ(p2, Point(3, 4));
        CHECK_EQ(p3.getX(), -3);
        CHECK_EQ(p3.getY(), 4);
        CHECK_EQ(p3, Point(-3, 4));
        CHECK_EQ(p4.getX(), 3);
        CHECK_EQ(p4.getY(), -4);
        CHECK_EQ(p4, Point(3, -4));
        CHECK_EQ(p5.getX(), -3);
        CHECK_EQ(p5.getY(), -4);
        CHECK_EQ(p5, Point(-3, -4));
    }

    TEST_CASE("test distance")
    {
        Point p1;
        Point p2(3, 4);
        Point p3(-3, 4);
        Point p4(3, -4);
        Point p5(-3, -4);

        CHECK_EQ(p1.distance(p1), 0);
        CHECK_EQ(p5.distance(p5), 0);

        CHECK_EQ(p1.distance(p2), 5);
        CHECK_EQ(p3.distance(p1), 5);
        CHECK_EQ(p1.distance(p4), 5);
        CHECK_EQ(p5.distance(p1), 5);

        CHECK_EQ(p2.distance(p5), 10);
        CHECK_EQ(p5.distance(p2), 10);
        CHECK_EQ(p3.distance(p4), 10);
        CHECK_EQ(p4.distance(p3), 10);
        CHECK_EQ(p2.distance(p3), 6);
        CHECK_EQ(p3.distance(p2), 6);
        CHECK_EQ(p4.distance(p5), 6);
        CHECK_EQ(p5.distance(p4), 6);
        CHECK_EQ(p2.distance(p4), 8);
        CHECK_EQ(p4.distance(p2), 8);
        CHECK_EQ(p3.distance(p5), 8);
        CHECK_EQ(p5.distance(p3), 8);

        CHECK_EQ(p1.distance(Point(12, 16)), 20);
        CHECK_EQ(Point(12, 16).distance(p1), 20);
        CHECK_EQ(Point(48, 64).distance(Point(-12, -16)), 100);
    }


    TEST_CASE("test distance and move towards")
    {
        Point location;
        Point flag1(30, 40);
        Point flag2(30, -40);
        Point flag3(-30, -40);

        CHECK_EQ(location.distance(flag1), 50);
        location = Point::moveTowards(location, flag1, 5);
        CHECK_EQ(location, Point(3,4));
        location = Point::moveTowards(location, flag1, 2.5);
        CHECK_EQ(location, Point(4.5, 6));
        location = Point::moveTowards(location, flag3, 2.5);
        CHECK_EQ(location, Point(3,4));
        CHECK_EQ(Point::moveTowards(location, flag1, 2.5), Point(4.5, 6));
        location = Point::moveTowards(location, flag1, 45);
        CHECK_EQ(location, Point(30, 40));

        CHECK_EQ(Point::moveTowards(location, location, 45), Point(30, 40));
        CHECK_EQ(Point::moveTowards(location, Point(0, 0), 0), Point(30, 40));
        CHECK_EQ(Point::moveTowards(location, Point(100, 100), 0), Point(30, 40));

        CHECK_THROWS(Point::moveTowards(location, location, -5));
        CHECK_THROWS(Point::moveTowards(location, Point(), -5));
        CHECK_THROWS(Point::moveTowards(location, flag3, -5));

        location = Point::moveTowards(location, flag2, 100);
        CHECK_EQ(location, flag2);
        location = Point::moveTowards(location, flag3, 100);
        CHECK_EQ(location, flag3);
        location = Point::moveTowards(location, Point(), 100);
        CHECK_EQ(location, Point(0, 0));

        CHECK_EQ(Point::moveTowards(Point(24, 0), Point(0, 0), 12), Point(12, 0));
        CHECK_EQ(Point::moveTowards(Point(0, 24), Point(0, 0), 12), Point(0, 12));
        CHECK_EQ(Point::moveTowards(Point(24, 0), Point(-10, 0), 12), Point(12, 0));
        CHECK_EQ(Point::moveTowards(Point(0, 24), Point(0, -10), 12), Point(0, 12));
        CHECK_EQ(Point::moveTowards(Point(-24, 0), Point(0, 0), 12), Point(-12, 0));
        CHECK_EQ(Point::moveTowards(Point(0, -24), Point(0, 0), 12), Point(0, -12));
        CHECK_EQ(Point::moveTowards(Point(-24, 0), Point(10, 0), 12), Point(-12, 0));
        CHECK_EQ(Point::moveTowards(Point(0, -24), Point(0, 10), 12), Point(0, -12));
    }
}

TEST_SUITE("Character unit test") {
    TEST_CASE("build")
    {
        Cowboy cowboy1("Cowboy 1", Point(-6, -8));
        YoungNinja ninja1("Ninja 1", Point(-6, 8));
        TrainedNinja ninja2("Ninja 2", Point(0, 8));
        OldNinja ninja3("Ninja 3", Point(6, 8));

        std::cout << "Cowboy 1: " << cowboy1.print() << std::endl;
        std::cout << "Ninja 1: " << ninja1.print() << std::endl;
        std::cout << "Ninja 2: " << ninja2.print() << std::endl;
        std::cout << "Ninja 3: " << ninja3.print() << std::endl;

        CHECK_EQ(std::string(cowboy1.getName()), std::string("Cowboy 1"));
        CHECK_EQ(cowboy1.getLocation(), Point(-6, -8));
        CHECK_EQ(cowboy1.getHealth(), 110);
        CHECK(cowboy1.isAlive());
        CHECK(cowboy1.hasboolets());

        CHECK_EQ(std::string(ninja1.getName()), std::string("Ninja 1"));
        CHECK_EQ(ninja1.getLocation(), Point(-6, 8));
        CHECK_EQ(ninja1.getHealth(), 100);
        CHECK(ninja1.isAlive());
        CHECK_EQ(ninja2.getHealth(), 120);
        CHECK_EQ(ninja3.getHealth(), 150);

        CHECK_THROWS(Cowboy(nullptr, Point()));
        CHECK_THROWS(YoungNinja(nullptr, Point()));
        CHECK_THROWS(TrainedNinja(nullptr, Point()));
        CHECK_THROWS(OldNinja(nullptr, Point()));
    }

    TEST_CASE("Cowboy operations")
    {
        Cowboy cowboy1("Cowboy 1", Point(-1.5, 2));
        Cowboy cowboy2("Cowboy 2", Point(1.5, 2));
        Cowboy cowboy3("Cowboy 3", Point());
        Cowboy cowboy4("Temp Cowboy", Point(1000, 1000));

        CHECK_EQ(cowboy1.distance(cowboy2), 3);
        CHECK_EQ(cowboy2.distance(cowboy1), 3);

        cowboy3.hit(10);
        CHECK_EQ(cowboy3.getHealth(), 100);
        cowboy3.hit(30);
        CHECK_EQ(cowboy3.getHealth(), 70);

        CHECK_THROWS(cowboy3.hit(-10));

        cowboy3.hit(80);
        CHECK_EQ(cowboy3.getHealth(), 0);
        CHECK(!cowboy3.isAlive());

        CHECK_THROWS(cowboy1.shoot(nullptr));
        CHECK_THROWS(cowboy3.hit(10));
        CHECK_THROWS(cowboy3.hit(-10));
        CHECK_THROWS(cowboy3.shoot(&cowboy2));
        CHECK_THROWS(cowboy3.reload());

        cowboy1.shoot(&cowboy2);
        CHECK_EQ(cowboy2.getHealth(), 100);
        CHECK(cowboy1.hasboolets());
        cowboy2.shoot(&cowboy1);
        CHECK_EQ(cowboy1.getHealth(), 100);
        CHECK(cowboy2.hasboolets());
        cowboy1.shoot(&cowboy2);
        CHECK_EQ(cowboy2.getHealth(), 90);
        CHECK(cowboy1.hasboolets());
        cowboy1.shoot(&cowboy2);
        CHECK_EQ(cowboy2.getHealth(), 80);
        CHECK(cowboy1.hasboolets());
        cowboy1.shoot(&cowboy2);
        CHECK_EQ(cowboy2.getHealth(), 70);
        CHECK(cowboy1.hasboolets());
        cowboy1.shoot(&cowboy2);
        CHECK_EQ(cowboy2.getHealth(), 60);
        CHECK(cowboy1.hasboolets());
        cowboy1.shoot(&cowboy2);
        CHECK_EQ(cowboy2.getHealth(), 50);

        CHECK(!cowboy1.hasboolets());

        CHECK_THROWS(cowboy1.shoot(&cowboy2));

        cowboy1.reload();
        CHECK(cowboy1.hasboolets());

        cowboy1.shoot(&cowboy2);
        cowboy1.reload();
        cowboy1.shoot(&cowboy2);
        cowboy1.shoot(&cowboy2);
        cowboy1.shoot(&cowboy2);
        CHECK(cowboy2.isAlive());
        cowboy1.shoot(&cowboy2);
        CHECK(!cowboy2.isAlive());

        cowboy1.shoot(&cowboy4);
        CHECK(cowboy1.hasboolets());
        cowboy1.shoot(&cowboy4);
        CHECK(!cowboy1.hasboolets());

        cowboy1.reload();
        CHECK_EQ(cowboy1.getHealth(), 100);
        cowboy1.shoot(&cowboy1);
        cowboy1.shoot(&cowboy1);
        cowboy1.shoot(&cowboy1);
        CHECK_EQ(cowboy1.getHealth(), 70);
    }

    TEST_CASE("Ninja operations")
    {
        YoungNinja ninja1("Ninja 1", Point(-14, 0));
        TrainedNinja ninja2("Ninja 2", Point(-11.1, 0));
        OldNinja ninja3("Ninja 3", Point(-7, 0));
        OldNinja ninja4("Ninja 4", Point(-6.5, 0));
        YoungNinja ninja5("Ninja that kill herself", Point());
        OldNinja centerNinja("Center", Point(20, 0));
        OldNinja lostNinja("Load one", Point(-100, 0));

        ninja1.move(&centerNinja);
        ninja2.move(&centerNinja);
        ninja3.move(&centerNinja);
        ninja4.move(&centerNinja);

        CHECK(doctest::Approx(ninja2.getLocation().getX()).epsilon(0.001) == 0.9);
        CHECK(doctest::Approx(ninja3.getLocation().getX()).epsilon(0.001) == 1);
        CHECK(doctest::Approx(ninja4.getLocation().getX()).epsilon(0.001) == 1.5);

        CHECK_EQ(ninja1.distance(ninja4), 1.5);
        CHECK_EQ(ninja4.distance(ninja1), 1.5);

        ninja1.slash(&ninja4);
        ninja1.slash(&ninja3);
        ninja1.slash(&ninja2);
        ninja1.slash(&ninja1);

        CHECK_EQ(ninja1.getHealth(), 69);
        CHECK_EQ(ninja2.getHealth(), 89);
        CHECK_EQ(ninja3.getHealth(), 150);
        CHECK_EQ(ninja4.getHealth(), 150);

        ninja1.move(&ninja2);
        ninja1.slash(&ninja2);
        CHECK_EQ(ninja2.getHealth(), 58);

        ninja2.move(&lostNinja);
        ninja1.slash(&ninja2);
        CHECK_EQ(ninja2.getHealth(), 58);

        ninja1.slash(&ninja3);
        ninja1.slash(&ninja4);
        CHECK_EQ(ninja3.getHealth(), 119);
        CHECK_EQ(ninja4.getHealth(), 119);

        ninja3.slash(&ninja1);
        ninja4.slash(&ninja1);
        CHECK_EQ(ninja1.getHealth(), 7);
        CHECK(ninja1.isAlive());

        ninja1.slash(&ninja3);
        ninja1.slash(&ninja4);
        CHECK_EQ(ninja3.getHealth(), 88);
        CHECK_EQ(ninja4.getHealth(), 88);

        ninja3.slash(&ninja1);
        CHECK_EQ(ninja1.getHealth(), 0);
        CHECK(!ninja1.isAlive());

        ninja5.slash(&ninja5);
        CHECK_EQ(ninja5.getHealth(), 69);
        ninja5.slash(&ninja5);
        CHECK_EQ(ninja5.getHealth(), 38);
        ninja5.slash(&ninja5);
        CHECK_EQ(ninja5.getHealth(), 7);
        ninja5.slash(&ninja5);
        CHECK_EQ(ninja5.getHealth(), 0);
        CHECK_EQ(ninja5.getHealth(), 0);
        CHECK_THROWS(ninja5.slash(&ninja5));

        CHECK_THROWS(ninja5.move(nullptr));
        CHECK_THROWS(ninja5.slash(nullptr));
    }
}

TEST_SUITE("Team unit test") {
    TEST_CASE("build")
    {
        Cowboy leader1("Leader of Team 1", Point(1, 0));
        Cowboy leader2("Leader of Team 2", Point(2, 0));
        Cowboy leader3("Leader of Team 3", Point(3, 0));
        YoungNinja* leader4 = new YoungNinja("Leader of Team 4", Point(4, 0));
        TrainedNinja* leader5 = new TrainedNinja("Leader of Team 5", Point(5, 0));
        OldNinja* leader6 = new OldNinja("Leader of Team 6", Point(6, 0));

        Team team1(&leader1);
        Team2 team2(&leader2);
        SmartTeam team3(&leader3);
        Team team4(leader4);
        Team2 team5(leader5);
        SmartTeam team6(leader6);

        team1.print();
        team2.print();
        team3.print();

        CHECK_THROWS(Team(nullptr));
        CHECK_THROWS(Team2(nullptr));
        CHECK_THROWS(SmartTeam(nullptr));
        CHECK_THROWS(Team(&leader1));
        CHECK_THROWS(Team2(&leader1));
        CHECK_THROWS(SmartTeam(&leader1));
        CHECK_THROWS(Team(&leader2));
        CHECK_THROWS(Team2(&leader2));
        CHECK_THROWS(SmartTeam(&leader2));
        CHECK_THROWS(Team(&leader3));
        CHECK_THROWS(Team2(&leader3));
        CHECK_THROWS(SmartTeam(&leader3));
        CHECK_THROWS(Team(leader4));
        CHECK_THROWS(Team2(leader5));
        CHECK_THROWS(SmartTeam(leader6));

        CHECK_THROWS(team1.add(nullptr));
        CHECK_THROWS(team2.add(nullptr));
        CHECK_THROWS(team3.add(nullptr));
        CHECK_THROWS(team1.add(&leader1));
        CHECK_THROWS(team1.add(leader5));
        CHECK_THROWS(team2.add(&leader2));
        CHECK_THROWS(team2.add(leader6));
        CHECK_THROWS(team3.add(&leader3));
        CHECK_THROWS(team3.add(leader4));

        Cowboy corp("corp", Point());
        corp.hit(1000);
        CHECK_THROWS(team1.add(&corp));

        Team* teams[] = {&team1, &team2, &team3, &team4, &team5, &team6};

        const int N = 6000;
        Character* characters[N];

        for (int i = 0; i < N; i++)
        {
            int teamIndex = randInteger(0, 5);
            if (randInteger(0, 1) == 0)
            {
                characters[i] = new Cowboy("Mr bot", Point(i, i));
            }
            else
            {
                characters[i] = new OldNinja("Mr bot", Point(i, i));
            }

            teams[teamIndex]->add(characters[i]);
        }

        int sum = 0;
        for (int i = 0; i < 6; i++)
        {
            sum += teams[i]->stillAlive();
        }

        CHECK_EQ(sum, N + 6);

        int alive = N + 6;
        for (int i = 0; i < N; i++)
        {
            if (randInteger(0, 1) == 0)
            {
                characters[i]->hit(1000);
                alive--;
            }
        }

        sum = 0;
        for (int i = 0; i < 6; i++)
        {
            sum += teams[i]->stillAlive();
        }

        CHECK_EQ(sum, alive);
    }

    TEST_CASE("Team against it self")
    {
        Cowboy* leader1 = new Cowboy("Leader of Team 1", Point(1, 0));
        Team team1(leader1);
        CHECK_THROWS(team1.attack(&team1));

        YoungNinja leader2("Leader of Team 2", Point(4, 0));
        Team2 team2(&leader2);
        CHECK_THROWS(team2.attack(&team2));

        TrainedNinja* leader3 = new TrainedNinja( "Leader of Team 3", Point(5, 0));
        SmartTeam team3(leader3);
        CHECK_THROWS(team3.attack(&team3));
    }

    TEST_CASE("Simple teams, single cowboy vs single cowboy")
    {
        for (int k = 0; k < 2; k++)
        {
            Cowboy botA("Bot A1", Point());
            Team* teamA = (k == 0) ? new Team(&botA) : new Team2(&botA);

            Cowboy botB("Bot B1", Point(100, 100));
            Team* teamB = (k == 0) ? new Team(&botB) : new Team2(&botB);

            CHECK_EQ(teamA->stillAlive(), 1);
            CHECK_EQ(teamB->stillAlive(), 1);

            for (int i = 0; i < 6; i++)
            {
                teamA->attack(teamB);
                teamB->attack(teamA);
            }

            CHECK_EQ(botA.getHealth(), 50);
            CHECK_EQ(botB.getHealth(), 50);
            CHECK(!botA.hasboolets());
            CHECK(!botB.hasboolets());

            teamA->attack(teamB);
            teamB->attack(teamA);

            CHECK_EQ(botA.getHealth(), 50);
            CHECK_EQ(botB.getHealth(), 50);
            CHECK(botA.hasboolets());
            CHECK(botB.hasboolets());

            for (int i = 0; i < 4; i++)
            {
                teamA->attack(teamB);
                teamB->attack(teamA);
            }

            CHECK_EQ(teamA->stillAlive(), 1);
            CHECK_EQ(teamB->stillAlive(), 1);

            CHECK_EQ(botA.getHealth(), 10);
            CHECK_EQ(botB.getHealth(), 10);

            teamA->attack(teamB);
            CHECK_EQ(teamB->stillAlive(), 0);
            CHECK_THROWS(teamB->attack(teamA));
        }
    }

    TEST_CASE("Simple teams, ninja cowboy vs ninja cowboy")
    {
        for (int k = 0; k < 2; k++) {
            TrainedNinja botA("Bot A1", Point(0, 24.25));
            Team* teamA = (k == 0) ? new Team(&botA) : new Team2(&botA);

            TrainedNinja botB("Bot B1", Point(0, -24.25));
            Team* teamB = (k == 0) ? new Team(&botB) : new Team2(&botB);

            CHECK_EQ(teamA->stillAlive(), 1);
            CHECK_EQ(teamB->stillAlive(), 1);

            CHECK_EQ(botA.distance(botB), 48.5);

            teamA->attack(teamB);
            teamB->attack(teamA);

            CHECK_EQ(botA.distance(botB), 24.5);

            teamA->attack(teamB);
            teamB->attack(teamA);

            CHECK_EQ(botB.distance(botA), 0.5);

            CHECK_EQ(teamA->stillAlive(), 1);
            CHECK_EQ(botA.getHealth(), 120);
            CHECK_EQ(teamB->stillAlive(), 1);
            CHECK_EQ(botB.getHealth(), 120);

            teamA->attack(teamB);
            teamB->attack(teamA);

            CHECK_EQ(teamA->stillAlive(), 1);
            CHECK_EQ(botA.getHealth(), 89);
            CHECK_EQ(teamB->stillAlive(), 1);
            CHECK_EQ(botB.getHealth(), 89);

            teamA->attack(teamB);
            teamB->attack(teamA);
            teamA->attack(teamB);
            teamB->attack(teamA);

            CHECK_EQ(teamA->stillAlive(), 1);
            CHECK_EQ(botA.getHealth(), 27);
            CHECK_EQ(teamB->stillAlive(), 1);
            CHECK_EQ(botB.getHealth(), 27);

            teamA->attack(teamB);
            CHECK_EQ(teamB->stillAlive(), 0);
            CHECK_THROWS(teamB->attack(teamA));
        }
    }

    TEST_CASE("Team Cowboy Operations")
    {
        for (int k = 0; k < 2; k++) {
            Cowboy botA1("Bot A1", Point(0, 0));
            Cowboy botA2("Bot A2", Point(1, 1));
            Cowboy botA3("Bot A3", Point(2, 2));
            Cowboy botA4("Bot A4", Point(3, 3));
            Cowboy botA5("Bot A5", Point(4, 4));
            Cowboy botA6("Bot A6", Point(5, 5));
            Team* teamA = (k == 0) ? new Team(&botA1) : new Team2(&botA1);
            teamA->add(&botA2);
            teamA->add(&botA3);
            teamA->add(&botA4);
            teamA->add(&botA5);
            teamA->add(&botA6);

            Cowboy botB1("Bot B1", Point(81, 81));
            Cowboy botB2("Bot B2", Point(91, 91));
            Cowboy botB3("Bot B3", Point(93, 93));
            Cowboy botB4("Bot B4", Point(95, 95));
            Cowboy botB5("Bot B5", Point(97, 97));
            Cowboy botB6("Bot B6", Point(99, 99));
            Team* teamB = (k == 0) ? new Team(&botB1) : new Team2(&botB1);
            teamB->add(&botB2);
            teamB->add(&botB3);
            teamB->add(&botB4);
            teamB->add(&botB5);
            teamB->add(&botB6);

            CHECK_EQ(teamA->stillAlive(), 6);
            CHECK_EQ(teamB->stillAlive(), 6);

            teamA->attack(teamB);
            teamB->attack(teamA);

            CHECK_EQ(teamA->stillAlive(), 6);
            CHECK_EQ(teamB->stillAlive(), 6);
            CHECK_EQ(botB1.getHealth(), 50);
            CHECK_EQ(botA6.getHealth(), 50);

            teamA->attack(teamB);
            teamB->attack(teamA);

            CHECK_EQ(teamA->stillAlive(), 5);
            CHECK_EQ(teamB->stillAlive(), 5);
            CHECK(!botB1.isAlive());
            CHECK_EQ(botB2.getHealth(), 100);
            CHECK(!botA6.isAlive());
            CHECK_EQ(botA5.getHealth(), 110);
        }
    }

    TEST_CASE("Team Ninja Operations")
    {
        for (int k = 0; k < 2; k++) {
            TrainedNinja botA1("Bot A1", Point(0, 0));
            TrainedNinja botA2("Bot A2", Point(0, -0.1));
            TrainedNinja botA3("Bot A3", Point(0, -0.2));
            Team* teamA = (k == 0) ? new Team(&botA1) : new Team2(&botA1);
            teamA->add(&botA2);
            teamA->add(&botA3);

            TrainedNinja botB1("Bot B1", Point(0, 72));
            TrainedNinja botB2("Bot B2", Point(0, 72.1));
            TrainedNinja botB3("Bot B3", Point(0, 72.2));
            Team* teamB = (k == 0) ? new Team(&botB1) : new Team2(&botB1);
            teamB->add(&botB2);
            teamB->add(&botB3);

            CHECK_EQ(teamA->stillAlive(), 3);
            CHECK_EQ(teamB->stillAlive(), 3);

            teamA->attack(teamB);
            teamB->attack(teamA);
            teamA->attack(teamB);
            teamB->attack(teamA);
            teamA->attack(teamB);
            teamB->attack(teamA);

            CHECK_EQ(teamA->stillAlive(), 3);
            CHECK_EQ(teamB->stillAlive(), 3);
            CHECK_EQ(botA1.distance(botB1), 0);

            teamA->attack(teamB);
            teamB->attack(teamA);
            teamA->attack(teamB);
            teamB->attack(teamA);

            CHECK_EQ(teamA->stillAlive(), 2);
            CHECK(!botA1.isAlive());
            CHECK_EQ(teamB->stillAlive(), 2);
            CHECK(!botB1.isAlive());
        }
    }

    TEST_CASE("Add dead's to Team")
    {
        TrainedNinja botA("Bot A", Point(0, 0));
        TrainedNinja botB("Bot B", Point(0, 0));
        TrainedNinja botC("Bot C", Point(0, 0));
        TrainedNinja dead("dead", Point(0, 0));
        dead.hit(1000);

        CHECK_THROWS(new Team(&dead));
        CHECK_THROWS(new Team2(&dead));
        CHECK_THROWS(new SmartTeam(&dead));

        Team teamA(&botA);
        Team2 teamB(&botB);
        SmartTeam teamC(&botC);

        CHECK_THROWS(teamA.add(&dead));
        CHECK_THROWS(teamB.add(&dead));
        CHECK_THROWS(teamC.add(&dead));
    }

    TEST_CASE("Team alive counter")
    {
        for (int i = 0; i < 3; ++i) {
            TrainedNinja bot1("Bot 1", Point(0, 0));
            TrainedNinja bot2("Bot 2", Point(0, 0));
            TrainedNinja bot3("Bot 3", Point(0, 0));

            Team* team;
            if (i == 0) {
                team = new Team(&bot1);
            } else if (i == 1) {
                team = new Team2(&bot1);
            } else {
                team = new SmartTeam(&bot1);
            }

            CHECK_EQ(team->stillAlive(), 1);

            team->add(&bot2);
            team->add(&bot3);

            CHECK_EQ(team->stillAlive(), 3);

            bot2.hit(1000);

            CHECK_EQ(team->stillAlive(), 2);

            bot1.hit(1000);

            CHECK_EQ(team->stillAlive(), 1);

            bot3.hit(1000);

            CHECK_EQ(team->stillAlive(), 0);
        }
    }
}

TEST_SUITE("E2E Tests")
{
    TEST_CASE("Simple 4 teams battle")
    {
        Cowboy botA1("Cowboy A", Point(2.4, 2.4));
        TrainedNinja botA2("Ninja A", Point(0, 0));
        Team* teamA = new Team(&botA1);
        teamA->add(&botA2);

        Cowboy botB1("Cowboy B", Point(2.6, 2.4));
        TrainedNinja botB2("Ninja B", Point(5, 0));
        Team* teamB = new Team(&botB1);
        teamB->add(&botB2);

        Cowboy botC1("Cowboy C", Point(2.6, 2.6));
        TrainedNinja botC2("Ninja C", Point(5, 5));
        Team* teamC = new Team(&botC1);
        teamC->add(&botC2);

        Cowboy botD1("Cowboy D", Point(2.4, 2.6));
        TrainedNinja botD2("Ninja D", Point(0, 5));
        Team* teamD = new Team(&botD1);
        teamD->add(&botD2);

        teamA->attack(teamB);
        teamB->attack(teamC);
        teamC->attack(teamD);
        teamD->attack(teamA);

        CHECK_EQ(botA1.getHealth(), 100);
        CHECK_EQ(botB1.getHealth(), 100);
        CHECK_EQ(botC1.getHealth(), 100);
        CHECK_EQ(botD1.getHealth(), 100);

        teamA->attack(teamB);
        teamB->attack(teamC);
        teamC->attack(teamD);
        teamD->attack(teamA);

        CHECK_EQ(botA1.getHealth(), 59);
        CHECK_EQ(botB1.getHealth(), 59);
        CHECK_EQ(botC1.getHealth(), 59);
        CHECK_EQ(botD1.getHealth(), 59);

        teamA->attack(teamB);
        teamB->attack(teamC);
        teamC->attack(teamD);
        teamD->attack(teamA);

        CHECK_EQ(botA1.getHealth(), 18);
        CHECK_EQ(botB1.getHealth(), 18);
        CHECK_EQ(botC1.getHealth(), 18);
        CHECK_EQ(botD1.getHealth(), 18);

        teamA->attack(teamB);
        teamB->attack(teamC);
        teamC->attack(teamD);
        teamD->attack(teamA);

        CHECK(!botA1.isAlive());
        CHECK_EQ(botA2.getHealth(), 120);
        CHECK(!botB1.isAlive());
        CHECK_EQ(botB2.getHealth(), 120);
        CHECK(!botB1.isAlive());
        CHECK_EQ(botC2.getHealth(), 120);
        CHECK(!botB1.isAlive());
        CHECK_EQ(botD2.getHealth(), 120);

        for (int i = 0; i < 3; ++i) {
            teamA->attack(teamB);
            teamB->attack(teamC);
            teamC->attack(teamD);
            teamD->attack(teamA);
        }

        CHECK_EQ(botA2.getHealth(), 27);
        CHECK_EQ(botB2.getHealth(), 27);
        CHECK_EQ(botC2.getHealth(), 27);
        CHECK_EQ(botD2.getHealth(), 27);

        teamA->attack(teamB);
        teamC->attack(teamD);
        teamA->attack(teamC);

        CHECK_EQ(teamA->stillAlive(), 1);
        CHECK_EQ(teamB->stillAlive(), 0);
        CHECK_EQ(teamC->stillAlive(), 0);
        CHECK_EQ(teamD->stillAlive(), 0);

        CHECK_THROWS(teamB->attack(teamA));
        CHECK_THROWS(teamA->attack(teamB));

        teamA->print();
        teamB->print();
    }

    TEST_CASE("Simple")
    {
        Cowboy botA1("Bot A1", Point(0, 0));
        Cowboy botA2("Bot A2", Point(-1, 0));
        Cowboy botA3("Bot A3", Point(-2, 0));
        Cowboy botA4("Bot A4", Point(-3, 0));
        Cowboy botA5("Bot A5", Point(-4, 0));
        Cowboy botA6("Bot A6", Point(-5, 0));
        Team* teamA = new Team(&botA1);
        teamA->add(&botA2);
        teamA->add(&botA3);
        teamA->add(&botA4);
        teamA->add(&botA5);
        teamA->add(&botA6);

        YoungNinja botB1("Bot B1", Point(0, 0));
        YoungNinja botB2("Bot B2", Point(28, 0));
        TrainedNinja botB3("Bot B3", Point(0, 0));
        TrainedNinja botB4("Bot B4", Point(24, 0));
        OldNinja botB5("Bot B5", Point(0, 0));
        OldNinja botB6("Bot B6", Point(16, 0));
        Team* teamB = new Team(&botB1);
        teamB->add(&botB2);
        teamB->add(&botB3);
        teamB->add(&botB4);
        teamB->add(&botB5);
        teamB->add(&botB6);

        CHECK_EQ(teamA->stillAlive(), 6);
        CHECK_EQ(teamB->stillAlive(), 6);

        teamA->attack(teamB);
        teamB->attack(teamA);

        CHECK_EQ(botA1.getHealth(), 17);
        CHECK_EQ(botB1.getHealth(), 40);

        teamA->print();
        teamB->print();
    }
}
