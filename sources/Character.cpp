#include "Character.hpp"

namespace ariel {
    unsigned int Character::s_ids = 0;

    double Character::distance(const Character* other) const {
        if (other == nullptr) {
            throw std::invalid_argument("other cannot be null");
        }
        return m_location.distance(other->m_location);
    }

    void Character::hit(int damage) {
        if (damage < 0) {
            throw std::invalid_argument ("Damage has to be positive number");
        }

        if (!isAlive()) {
            throw std::runtime_error("Cant hit a deed Character!");
        }

        if (damage < m_health) {
            m_health -= damage;
        } else {
            m_health = 0;
        }
    }
}
