#pragma once

#include "Ninja.hpp"

namespace ariel {
    class YoungNinja : public Ninja {
        static const int STARTING_HEALTH = 100;
        static const int STARTING_SPEED = 14;

    public:
        YoungNinja() = delete;
        YoungNinja(const YoungNinja& other) = delete;
        YoungNinja(const YoungNinja&& other) = delete;

        YoungNinja(const char *name, const Point &location)
                : Ninja(name, location, STARTING_HEALTH, STARTING_SPEED) { }

        ~YoungNinja() override { }

        YoungNinja& operator=(YoungNinja& other) = delete;
        YoungNinja& operator=(YoungNinja&& other) = delete;
    };
}
