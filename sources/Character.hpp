#pragma once

#include <string>
#include <sstream>
#include <stdexcept>

#include "Point.hpp"

namespace ariel {
    class Character {
    private:
        static unsigned int s_ids; // NOLINT

        unsigned int m_id;
        const char *m_name;
        Point m_location;
        int m_health;
        bool m_memberOfTeam;

    public:
        Character() = delete;
        Character(const Character& other) = delete;
        Character(const Character&& other) = delete;

        Character& operator=(Character& other) = delete;
        Character& operator=(Character&& other) = delete;

    protected:
        Character(const char *name, const Point &location, int health)
                : m_id(s_ids), m_name(name), m_location(location), m_health(health), m_memberOfTeam(false) {
            if (name == nullptr) {
                throw std::invalid_argument("Name cannot be null");
            }

            if (health < 0) {
                throw std::runtime_error("Health has to be positive number");
            }

            s_ids++;
        }

    public:
        virtual ~Character() { }

        friend bool operator==(const Character &characterA, const Character &characterB) {
            return characterA.m_id == characterB.m_id;
        }

        inline Point getLocation() const { return m_location; }

        inline int getHealth() const { return m_health; }

        inline const char *getName() const { return m_name; }
    protected:
        inline void setLocation(const Point& location) { m_location = location; }

    public:
        inline bool isAlive() const { return m_health > 0; }

        double distance(const Character* other) const;

        void hit(int damage);

        virtual std::string toString() const { return ""; }

        std::string print() const { return toString(); }

        void addToTeam() {
            if (m_memberOfTeam) {
                throw std::runtime_error("Is already in team");
            }
            m_memberOfTeam = true;
        }

        inline bool inTeam() const { return m_memberOfTeam; }
    };
}
