#include "Point.hpp"

#include <cmath>
#include <sstream>

namespace ariel {
    Point Point::moveTowards(const Point &src, const Point &target,
                             double distance) {
        if (distance < 0) {
            throw std::invalid_argument("Can't move backwards!");
        }

        if (distance == 0) {
            return src;
        }

        double fullDistance = src.distance(target);

        if (fullDistance <= distance) {
            return target;
        }

        double x = src.m_x + (target.m_x - src.m_x) * (distance / fullDistance);
        double y = src.m_y + (target.m_y - src.m_y) * (distance / fullDistance);

        return Point(x, y);
    }

    Point::Point()
        : m_x(0), m_y(0) {
    }

    Point::Point(double x, double y)
        : m_x(x), m_y(y) {
    }

    double Point::distance(const Point &other) const {
        return std::sqrt(std::pow(m_x - other.m_x, 2) + std::pow(m_y - other.m_y, 2));
    }

    std::string Point::toString() const {
        std::stringstream stream;
        stream << "(" << m_x << ", " << m_y << ")";
        return stream.str();
    }
} // namespace ariel
