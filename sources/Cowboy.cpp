#include "Cowboy.hpp"

namespace ariel {
    void Cowboy::shoot(Character *enemy) {
        if (enemy == nullptr) {
            throw std::invalid_argument("enemy cannot be null!");
        }

        if (*enemy == *this) {
            throw std::runtime_error("cannot shoot himself!");
        }

        if (!isAlive()) {
            throw std::runtime_error("dead cannot be shoot!");
        }

        if (!enemy->isAlive()) {
            throw std::runtime_error("Cant shoot a deed enemy!");
        }

        if (hasboolets()) {
            enemy->hit(DAMAGE);
            m_boolets--;
        }
    }

    void Cowboy::reload() {
        if (!isAlive()) {
            throw std::runtime_error("dead cannot reload!");
        }

        m_boolets = Cowboy::MAX_BOOLETS;
    }
}
