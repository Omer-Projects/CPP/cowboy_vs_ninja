#pragma once

#include "Team.hpp"

namespace ariel {
    class SmartTeam : public Team {
    public:
        SmartTeam() = delete;
        SmartTeam(const SmartTeam& other) = delete;
        SmartTeam(const SmartTeam&& other) = delete;

        SmartTeam(Character *leader)
                : Team(leader) {}

        ~SmartTeam() override { }

        SmartTeam& operator=(SmartTeam& other) = delete;
        SmartTeam& operator=(SmartTeam&& other) = delete;

        void attack(Team *enemyTeam) override;

        void print() const override;

    private:
        static void getAliveFighters(const Team& team, std::vector<Cowboy*>& cowboys, std::vector<Ninja*>& ninjas);

        void simpleKills(Team* enemyTeam, std::vector<Cowboy*>& activeCowboys, std::vector<Ninja*>& activeNinjas);
    };
}
