#pragma once

#include <string>

namespace ariel {

    class Point {
    public:
        static Point moveTowards(const Point &src, const Point &target,
                                 double distance);

    private:
        double m_x, m_y;

    public:
        Point();

        Point(double x, double y); // NOLINT

        inline double getX() const { return m_x; }

        inline double getY() const { return m_y; }

        double distance(const Point &other) const;

        std::string toString() const;

        std::string print() const { return toString(); }

        friend bool operator==(const Point &pointA, const Point &pointB) {
            return pointA.m_x == pointB.m_x && pointA.m_y == pointB.m_y;
        }

        friend std::ostream& operator<<(std::ostream& stream, const Point &point) {
            stream << point.toString();
            return stream;
        }
    };
}
