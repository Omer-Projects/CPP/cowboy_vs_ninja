#pragma once

#include <vector>
#include <stdexcept>
#include <iostream>

#include "Character.hpp"
#include "Cowboy.hpp"
#include "YoungNinja.hpp"
#include "TrainedNinja.hpp"
#include "OldNinja.hpp"

namespace ariel {

    class Team {
    public:
        static const int MAX_TEAM_SIZE = 10;
    private:
        static unsigned int s_ids; // NOLINT

        unsigned int m_id;
        Character *m_leader;
        std::vector<Character *> m_fighters;

    public:
        Team() = delete;
        Team(const Team& other) = delete;
        Team(const Team&& other) = delete;

        Team(Character *leader);

        virtual ~Team();

        Team& operator=(Team& other) = delete;
        Team& operator=(Team&& other) = delete;

        friend bool operator==(const Team &teamA, const Team &teamB) {
            return teamA.m_id == teamB.m_id;
        }

    protected:
        inline Character* getLeader() const { return m_leader; }
        inline void setLeader(Character* leader) { m_leader = leader; }

    public:
        inline const std::vector<Character *>& getFighters() const { return m_fighters; }
        inline std::vector<Character *>& getFighters() { return m_fighters; }

        void add(Character *fighter);

        int stillAlive() const;

        virtual void attack(Team *enemyTeam);
        virtual void print() const;

    protected:
        Character* closetTarget(Team* enemyTeam) const;

    private:
        void updateLeader();
        void attackWithCowboys(Team *enemyTeam);
        void attackWithNinjas(Team *enemyTeam);
    };
}
