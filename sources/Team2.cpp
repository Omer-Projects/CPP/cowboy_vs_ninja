#include "Team2.hpp"

namespace ariel {
    void Team2::attack(Team *enemyTeam) {
        if (enemyTeam == nullptr) {
            throw std::invalid_argument("enemy team cannot be null!");
        }

        if (stillAlive() == 0) {
            throw std::runtime_error("team cannot attack if all the Team dead!");
        }

        if (enemyTeam->stillAlive() == 0) {
            throw std::runtime_error("cannot attack dead Team!");
        }

        if (*enemyTeam == *this) {
            throw std::runtime_error("cannot attack itself!");
        }

        updateLeader();
        doAttack(enemyTeam);
    }

    void Team2::print() const {
        std::cout << "Team members:" << std::endl;

        for (Character *character: getFighters()) {
            std::cout << character->toString() << std::endl;
        }
    }

    void Team2::updateLeader() {
        if (!getLeader()->isAlive()) {
            double minDis = -1;
            Character *newLeader = nullptr;
            for (Character* fighter : getFighters()) {
                if (fighter->isAlive()) {
                    double dis = getLeader()->distance(fighter);
                    if (dis < minDis || newLeader == nullptr) {
                        minDis = dis;
                        newLeader = fighter;
                    }
                }
            }
            setLeader(newLeader);
        }
    }

    void Team2::doAttack(Team *enemyTeam) {
        Character *target = closetTarget(enemyTeam);

        for (Character *character: getFighters()) {
            if (target == nullptr) {
                break;
            }

            if (character->isAlive()) {
                Cowboy *cowboy = dynamic_cast<Cowboy *>(character);
                if (cowboy != nullptr) {
                    if (cowboy->hasboolets()) {
                        cowboy->shoot(target);
                        if (!target->isAlive()) {
                            target = closetTarget(enemyTeam);
                        }
                    } else {
                        cowboy->reload();
                    }
                } else {
                    Ninja *ninja = dynamic_cast<Ninja *>(character);
                    if (ninja->distance(target) < 1) {
                        ninja->slash(target);
                        if (!target->isAlive()) {
                            target = closetTarget(enemyTeam);
                        }
                    } else {
                        ninja->move(target);
                    }
                }
            }
        }
    }
}
