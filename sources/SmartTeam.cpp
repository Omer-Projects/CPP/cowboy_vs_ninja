#include "SmartTeam.hpp"

#include <algorithm>

namespace ariel {
    void SmartTeam::attack(Team *enemyTeam) {
        if (enemyTeam == nullptr) {
            throw std::invalid_argument("enemy team cannot be null!");
        }

        if (stillAlive() == 0) {
            throw std::runtime_error("team cannot attack if all the Team dead!");
        }

        if (enemyTeam->stillAlive() == 0) {
            throw std::runtime_error("cannot attack dead Team!");
        }

        if (*enemyTeam == *this) {
            throw std::runtime_error("cannot attack itself!");
        }

        // the attack
        std::vector<Cowboy*> activeCowboys;
        std::vector<Ninja*> activeNinjas;

        std::vector<Cowboy*> enemyCowboys;
        std::vector<Ninja*> enemyNinjas;

        getAliveFighters(*this, activeCowboys, activeNinjas);

        simpleKills(enemyTeam, activeCowboys, activeNinjas);

        for (Cowboy* cowboy: activeCowboys) {
            if (enemyTeam->stillAlive() == 0) {
                return;
            }

            getAliveFighters(*enemyTeam, enemyCowboys, enemyNinjas);

            Character* target = (enemyNinjas.size() > 0) ? (Character*)enemyNinjas[0] : (Character*)enemyCowboys[0];

            for (Cowboy* en : enemyCowboys) {
                if (en->getHealth() <= target->getHealth() && !en->hasboolets()) {
                    target = en;
                }
            }

            for (Cowboy* en : enemyCowboys) {
                if (en->getHealth() <= target->getHealth() && en->hasboolets()) {
                    target = en;
                }
            }


            for (Ninja* en : enemyNinjas) {
                if (en->getHealth() <= target->getHealth()) {
                    target = en;
                }
            }

            cowboy->shoot(target);
        }

        if (enemyTeam->stillAlive() == 0) {
            return;
        }

        getAliveFighters(*enemyTeam, enemyCowboys, enemyNinjas);

        for (Ninja* ninja: activeNinjas) {
            Character* target = (enemyNinjas.size() > 0) ? (Character*)enemyNinjas[0] : (Character*)enemyCowboys[0];

            for (Cowboy* en : enemyCowboys) {
                if (ninja->distance(en) < ninja->distance(target)) {
                    target = en;
                }
            }


            for (Ninja* en : enemyNinjas) {
                if (ninja->distance(en) < ninja->distance(target)) {
                    target = en;
                }
            }

            ninja->move(target);
        }
    }

    void SmartTeam::print() const {
        std::cout << "Team members:" << std::endl;
        for (Character *character: getFighters()) {
            Cowboy *cowboy = dynamic_cast<Cowboy *>(character);
            if (character->isAlive() && cowboy != nullptr) {
                std::cout << character->toString() << std::endl;
            }
        }

        for (Character *character: getFighters()) {
            Ninja *ninja = dynamic_cast<Ninja *>(character);
            if (character->isAlive() && ninja != nullptr) {
                std::cout << character->toString() << std::endl;
            }
        }

        for (Character *character: getFighters()) {
            if (!character->isAlive()) {
                std::cout << character->toString() << std::endl;
            }
        }
    }

    void SmartTeam::getAliveFighters(const Team& team, std::vector<Cowboy*>& cowboys, std::vector<Ninja*>& ninjas) {
        cowboys.clear();
        ninjas.clear();

        for (Character *character: team.getFighters()) {
            Cowboy *cowboy = dynamic_cast<Cowboy *>(character);
            if (character->isAlive() && cowboy != nullptr) {
                cowboys.push_back(cowboy);
            }
        }

        for (Character *character: team.getFighters()) {
            Ninja *ninja = dynamic_cast<Ninja *>(character);
            if (character->isAlive() && ninja != nullptr) {
                ninjas.push_back(ninja);
            }
        }
    }

    void SmartTeam::simpleKills(Team* enemyTeam, std::vector<Cowboy*>& activeCowboys, std::vector<Ninja*>& activeNinjas) {
        std::vector<Cowboy*> enemyCowboys;
        std::vector<Ninja*> enemyNinjas;
        getAliveFighters(*enemyTeam, enemyCowboys, enemyNinjas);

        for (Ninja* ninja : activeNinjas) {
            Character* target = nullptr;

            for (Ninja* en : enemyNinjas) {
                if (ninja->distance(en) < 1) {
                    if (target == nullptr) {
                        target = en;
                    } else if (target->getHealth() > Ninja::DAMAGE) {
                        if (target->getHealth() > en->getHealth()) {
                            target = en;
                        }
                    } else {
                        if (target->getHealth() < en->getHealth() && en->getHealth() <= Ninja::DAMAGE) {
                            target = en;
                        }
                    }
                }
            }

            for (Cowboy* en : enemyCowboys) {
                if (ninja->distance(en) < 1) {
                    if (target == nullptr) {
                        target = en;
                    } else if (target->getHealth() > Ninja::DAMAGE) {
                        if (target->getHealth() > en->getHealth()) {
                            target = en;
                        }
                    } else {
                        if (target->getHealth() < en->getHealth() && en->getHealth() <= Ninja::DAMAGE) {
                            target = en;
                        }
                    }
                }
            }

            if (target != nullptr) {
                ninja->slash(target);
                if (!target->isAlive()) {
                    getAliveFighters(*enemyTeam, enemyCowboys, enemyNinjas);
                }

                activeNinjas.erase(std::find(activeNinjas.begin(), activeNinjas.end(), ninja));
            }
        }

        std::vector<Character*> closeToDie;

        for (Ninja* ninja : enemyNinjas) {
            if (ninja->getHealth() <= Cowboy::DAMAGE) {
                closeToDie.push_back(ninja);
            }
        }

        for (Cowboy* cowboy : enemyCowboys) {
            if (cowboy->getHealth() <= Cowboy::DAMAGE && cowboy->hasboolets()) {
                closeToDie.push_back(cowboy);
            }
        }

        for (Cowboy* cowboy : enemyCowboys) {
            if (cowboy->getHealth() <= Cowboy::DAMAGE && !cowboy->hasboolets()) {
                closeToDie.push_back(cowboy);
            }
        }

        unsigned long targetNow = 0;
        for (Cowboy* cowboy : activeCowboys) {
            if (!cowboy->hasboolets()) {
                cowboy->reload();
                activeCowboys.erase(std::find(activeCowboys.begin(), activeCowboys.end(), cowboy));
            } else {
                if (targetNow < closeToDie.size()) {
                    cowboy->shoot(closeToDie.at(targetNow));
                    targetNow++;

                    activeCowboys.erase(std::find(activeCowboys.begin(), activeCowboys.end(), cowboy));
                }
            }
        }
    }
}
