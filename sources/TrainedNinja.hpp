#pragma once

#include "Ninja.hpp"

namespace ariel {
    class TrainedNinja : public Ninja {
        static const int STARTING_HEALTH = 120;
        static const int STARTING_SPEED = 12;

    public:
        TrainedNinja() = delete;
        TrainedNinja(const TrainedNinja& other) = delete;
        TrainedNinja(const TrainedNinja&& other) = delete;

        TrainedNinja(const char *name, const Point &location)
                : Ninja(name, location, STARTING_HEALTH, STARTING_SPEED) { }

        ~TrainedNinja() override { }

        TrainedNinja& operator=(TrainedNinja& other) = delete;
        TrainedNinja& operator=(TrainedNinja&& other) = delete;
    };
}
