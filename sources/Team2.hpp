#pragma once

#include "Team.hpp"

namespace ariel {
    class Team2 : public Team {
    public:
        Team2() = delete;
        Team2(const Team2& other) = delete;
        Team2(const Team2&& other) = delete;

        Team2(Character *leader)
                : Team(leader) { }

        ~Team2() override { }

        Team2& operator=(Team2& other) = delete;
        Team2& operator=(Team2&& other) = delete;

        void attack(Team *enemyTeam) override;

        void print() const override;

    private:
        void updateLeader();
        void doAttack(Team* enemyTeam);
    };
}
