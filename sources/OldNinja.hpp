#pragma once

#include "Ninja.hpp"

namespace ariel {
    class OldNinja : public Ninja {
        static const int STARTING_HEALTH = 150;
        static const int STARTING_SPEED = 8;

    public:
        OldNinja() = delete;
        OldNinja(const OldNinja& other) = delete;
        OldNinja(const OldNinja&& other) = delete;

        OldNinja(const char *name, const Point &location)
                : Ninja(name, location, STARTING_HEALTH, STARTING_SPEED) { }

        ~OldNinja() override { }

        OldNinja& operator=(OldNinja& other) = delete;
        OldNinja& operator=(OldNinja&& other) = delete;
    };
}
