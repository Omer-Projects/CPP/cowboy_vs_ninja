#pragma once

#include "Character.hpp"

namespace ariel {
    class Cowboy : public Character {
    public:
        static const int STARTING_HEALTH = 110;
        static const int MAX_BOOLETS = 6;
        static const int DAMAGE = 10;

    private:
        int m_boolets;

    public:
        Cowboy() = delete;
        Cowboy(const Cowboy& other) = delete;
        Cowboy(const Cowboy&& other) = delete;

        Cowboy(const char *name, const Point &location)
                : Character(name, location, STARTING_HEALTH), m_boolets(MAX_BOOLETS) { }

        ~Cowboy() override { }

        Cowboy& operator=(Cowboy& other) = delete;
        Cowboy& operator=(Cowboy&& other) = delete;

        std::string toString() const override {
            std::stringstream stream;

            if (!isAlive()) {
                stream << "(C " << getName() << "), " << getLocation().toString();
            } else {
                stream << "C " << getName() << ", " << getHealth() << ", " << getLocation().toString();
            }
            return stream.str();
        }

        void shoot(Character *enemy);

        bool hasboolets() const { return m_boolets > 0; }

        void reload();
    };
}
