#include "Ninja.hpp"

namespace ariel {
    void Ninja::move(Character *enemy) {
        if (enemy == nullptr) {
            throw std::invalid_argument("enemy cannot be null!");
        }

        if (!enemy->isAlive()) {
            throw std::runtime_error("Cant move to a deed enemy!");
        }

        if (!isAlive()) {
            throw std::runtime_error("dead cannot move!");
        }

        this->setLocation(Point::moveTowards(getLocation(), enemy->getLocation(), m_speed));
    }

    void Ninja::slash(Character *enemy) {
        if (enemy == nullptr) {
            throw std::invalid_argument("enemy cannot be null!");
        }

        if (*enemy == *this) {
            throw std::runtime_error("cannot slash himself!");
        }

        if (!enemy->isAlive()) {
            throw std::runtime_error("Cant slash a deed enemy!");
        }

        if (!isAlive()) {
            throw std::runtime_error("dead cannot slash!");
        }

        if (distance(enemy) < 1) {
            enemy->hit(DAMAGE);
        }
    }
}
