#pragma once

#include "Character.hpp"

namespace ariel {
    class Ninja : public Character {
    public:
        static const int DAMAGE = 40;
    private:
        int m_speed;

    public:
        Ninja() = delete;

        Ninja(const Ninja &other) = delete;

        Ninja(const Ninja &&other) = delete;

    protected:
        Ninja(const char *name, const Point &location, int health, int speed) // NOLINT
                : Character(name, location, health), m_speed(speed) {
            if (speed <= 0) {
                throw std::runtime_error("Speed is positive number");
            }
        }

    public:
        ~Ninja() override {}

        Ninja &operator=(Ninja &other) = delete;

        Ninja &operator=(Ninja &&other) = delete;

        std::string toString() const override {
            std::stringstream stream;
            if (!isAlive()) {
                stream << "(N " << getName() << "), " << getLocation().toString();
            } else {
                stream << "N " << getName() << ", " << getHealth() << ", " << getLocation().toString();
            }
            return stream.str();
        };

        void move(Character *enemy);

        void slash(Character *enemy);
    };
}
