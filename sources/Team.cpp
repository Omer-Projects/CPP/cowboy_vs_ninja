#include "Team.hpp"

#include <iostream>

namespace ariel {
    unsigned int Team::s_ids = 0;

    Team::Team(Character *leader)
            : m_leader(leader), m_id(s_ids) {
        if (leader == nullptr) {
            throw std::invalid_argument("leader cannot be null!");
        }

        if (!leader->isAlive()) {
            throw std::runtime_error("leader cannot be deed!");
        }

        if (leader->inTeam()) {
            throw std::runtime_error("leader is already in other team!");
        }

        s_ids += 1;

        leader->addToTeam();
        m_fighters.push_back(leader);
    }

    Team::~Team() {
        for (Character* fighter : m_fighters) {
            delete fighter;
        }
    }

    void Team::add(Character *fighter) {
        if (fighter == nullptr) {
            throw std::invalid_argument("fighter cannot be null!");
        }

        if (!fighter->isAlive()) {
            throw std::runtime_error("fighter cannot be deed!");
        }

        if (fighter->inTeam()) {
            throw std::runtime_error("fighter is already in other team!");
        }

        if (m_fighters.size() == MAX_TEAM_SIZE) {
            throw std::runtime_error("The team is in max capacity");
        }

        fighter->addToTeam();
        m_fighters.push_back(fighter);
    }

    int Team::stillAlive() const {
        int alive = 0;
        for (Character *character: m_fighters) {
            if (character->isAlive()) {
                alive++;
            }
        }
        return alive;
    }

    void Team::attack(Team *enemyTeam) {
        if (enemyTeam == nullptr) {
            throw std::invalid_argument("enemy team cannot be null!");
        }

        if (stillAlive() == 0) {
            throw std::runtime_error("team cannot attack if all the Team dead!");
        }

        if (enemyTeam->stillAlive() == 0) {
            throw std::runtime_error("cannot attack dead Team!");
        }

        if (*enemyTeam == *this) {
            throw std::runtime_error("cannot attack itself!");
        }

        updateLeader();

        attackWithCowboys(enemyTeam);
        attackWithNinjas(enemyTeam);
    }

    void Team::print() const {
        std::cout << "Team members:" << std::endl;

        for (Character *character: m_fighters) {
            if (dynamic_cast<Cowboy *>(character) != nullptr) {
                std::cout << character->toString() << std::endl;
            }
        }

        for (Character *character: m_fighters) {
            if (dynamic_cast<Ninja *>(character) != nullptr) {
                std::cout << character->toString() << std::endl;
            }
        }
    }

    Character *Team::closetTarget(Team *enemyTeam) const {
        Character *target = nullptr;
        double minDis = -1;
        for (Character *character: enemyTeam->m_fighters) {
            if (character->isAlive()) {
                double dis = m_leader->distance(character);
                if (dis < minDis || minDis == -1) {
                    minDis = dis;
                    target = character;
                }
            }
        }

        return target;
    }

    void Team::updateLeader() {
        if (!m_leader->isAlive()) {
            double minDis = -1;
            Character *newLeader = nullptr;
            for (Character* fighter : m_fighters) {
                if (fighter->isAlive()) {
                    double dis = m_leader->distance(fighter);
                    if (dis < minDis || newLeader == nullptr) {
                        minDis = dis;
                        newLeader = fighter;
                    }
                }
            }
            m_leader = newLeader;
        }
    }

    void Team::attackWithCowboys(Team *enemyTeam) {
        Character *target = closetTarget(enemyTeam);

        for (Character *character: m_fighters) {
            if (target == nullptr) {
                break;
            }

            Cowboy *cowboy = dynamic_cast<Cowboy *>(character);
            if (character->isAlive() && cowboy != nullptr) {
                if (cowboy->hasboolets()) {
                    cowboy->shoot(target);
                    if (!target->isAlive()) {
                        target = closetTarget(enemyTeam);
                    }
                } else {
                    cowboy->reload();
                }
            }
        }
    }

    void Team::attackWithNinjas(Team *enemyTeam) {
        Character *target = closetTarget(enemyTeam);

        for (Character *character: m_fighters) {
            if (target == nullptr) {
                break;
            }

            Ninja *ninja = dynamic_cast<Ninja *>(character);
            if (character->isAlive() && ninja != nullptr) {
                if (ninja->distance(target) < 1) {
                    ninja->slash(target);
                    if (!target->isAlive()) {
                        target = closetTarget(enemyTeam);
                    }
                } else {
                    ninja->move(target);
                }
            }
        }
    }
}
